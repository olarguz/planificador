package com.oagsoft.scheduler.time;

import java.util.Objects;

public class Time {
    private int hora;
    private int minuto;
    private int segundo;

    private Time() {
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public void setSegundo(int segundo) {
        this.segundo = segundo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Time tiempo = (Time) o;
        return hora == tiempo.hora &&
                minuto == tiempo.minuto &&
                segundo == tiempo.segundo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hora, minuto, segundo);
    }

    @Override
    public String toString() {
        return String.format("%2d:%02d:%02d", hora, minuto, segundo);
    }

    public static class Builder {
        private Time data;

        public Builder() {
            data = new Time();
        }

        public Builder withHora(int hora) {
            data.setHora(hora);
            return this;
        }

        public Builder withMinuto(int minuto) {
            data.setMinuto(minuto);
            return this;
        }

        public Builder withSegundo(int segundo) {
            data.setSegundo(segundo);
            return this;
        }

        public Time build() {
            return data;
        }
    }
}
