package com.oagsoft.scheduler;

import java.util.Timer;
import com.oagsoft.scheduler.task.Task;

public class Scheduler {
    private Timer time;
    public Scheduler(){
        time = new Timer();
    }
    public void addWork(Task task) {
        ScheduledTask st = new ScheduledTask();
        st.setTask(task);
        time.schedule(st, 0, 1000);
    }
}
