package com.oagsoft.scheduler;

import com.oagsoft.scheduler.time.Time;
import java.time.LocalDateTime;
import java.util.TimerTask;
import com.oagsoft.scheduler.task.Task;

public class ScheduledTask extends TimerTask {
    private Task task;

    public void setTask(Task task) {
        this.task = task;
    }

    public void run() {
        task.execute(f(LocalDateTime.now()));
    }

    Time f(LocalDateTime d) {
        return new Time.Builder()
                .withHora(d.getHour())
                .withMinuto(d.getMinute())
                .withSegundo(d.getSecond())
                .build();
    }
}
