package com.oagsoft.scheduler.task.impl;

import com.oagsoft.scheduler.task.Task;
import com.oagsoft.scheduler.time.Time;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Desarrollo
 */
public class TimeFileTask implements Task
{
    private String nombre;
    private String fileName;
    private List<Time> tiemposActivacion;
    private long ultimaFecha;

    public TimeFileTask(String nombre, String fileName, List<Time> tiemposActivacion) {
        this.nombre = nombre;
        this.fileName = fileName;
        this.tiemposActivacion = tiemposActivacion;
        this.ultimaFecha = leerModificacion("ultima.dta");
    }
    
    private long leerModificacion(String archivo) {
        long num = 0;
        try(BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            num = Long.parseLong(br.readLine());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TimeFileTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TimeFileTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return num;
    }
    
    private void escribirModificacion(String archivo,long fecha) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(archivo))) {
            bw.write(fecha+"");
            bw.close();
        }
        catch (IOException ex)
        {
            Logger.getLogger(TimeFileTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void execute(Time tActual)
    {
        if (!tiemposActivacion.stream().filter(t -> t.equals(tActual)).collect(Collectors.toList()).isEmpty()) {
            proceso();
        }
    }
    
    private void proceso () {
        File file = new File(fileName);
        long fechaLeida = file.lastModified();
         if ( fechaLeida != ultimaFecha){
             ultimaFecha = fechaLeida;
             escribirModificacion("ultima.dta",ultimaFecha);
         }
     }
}
