package com.oagsoft.scheduler.task.impl;

import com.oagsoft.scheduler.time.Time;
import java.util.List;
import java.util.stream.Collectors;
import com.oagsoft.scheduler.task.Task;

public class PrintingTask implements Task {
    private String nombre;
    private List<Time> tiemposActivacion;

    public PrintingTask(String nombre, List<Time> tiemposActivacion) {
        this.nombre = nombre;
        this.tiemposActivacion = tiemposActivacion;
    }

    public void execute(Time tActual) {
        if ( !tiemposActivacion.stream().filter(t -> t.equals(tActual)).collect(Collectors.toList()).isEmpty()) {
            System.out.println("Proceso En ejecucion '"+nombre+"'");
        }
    }
}
