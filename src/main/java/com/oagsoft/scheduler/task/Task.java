package com.oagsoft.scheduler.task;

import com.oagsoft.scheduler.time.Time;

public interface Task {
    void execute (Time tActual);
}
