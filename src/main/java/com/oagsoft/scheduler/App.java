package com.oagsoft.scheduler;

import com.oagsoft.scheduler.task.impl.CommandTask;
import com.oagsoft.scheduler.task.impl.PrintingTask;
import com.oagsoft.scheduler.task.impl.TimeFileTask;
import com.oagsoft.scheduler.time.Time;
import java.util.Arrays;

public class App {
    public static void main(String[] args) throws InterruptedException {
        Scheduler scheduler = new Scheduler();
        scheduler.addWork(new PrintingTask("Tarea 1", Arrays.asList(
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(30).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(0).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(15).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(20).build()
        )));
        scheduler.addWork(new PrintingTask("Tarea 2", Arrays.asList(
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(35).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(2).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(19).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(22).build()
        )));
        scheduler.addWork(new CommandTask("Tarea 3", Arrays.asList(
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(35).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(2).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(19).build(),
                new Time.Builder().withHora(15).withMinuto(8).withSegundo(22).build()
        )));
        scheduler.addWork(new TimeFileTask("Tarea 4", "SemCmd.zip", Arrays.asList(
                new Time.Builder().withHora(16).withMinuto(53).withSegundo(0).build(),
                new Time.Builder().withHora(16).withMinuto(53).withSegundo(30).build(),
                new Time.Builder().withHora(16).withMinuto(55).withSegundo(20).build()
        )));
    }
}
